import React from 'react';
import ReactDom from 'react-dom'
import Counter from "./components/counter";
import { createStore, bindActionCreators } from 'redux'
// import {inc, dec, rnd} from "./actions";
import * as actions from "./actions"
import {reducer} from "./reducer";
import { Provider } from 'react-redux'
import App from "./components/app";



const store = createStore(reducer);
// const {dispatch} = store;
//
// const {inc, dec, rnd} = bindActionCreators( actions, dispatch);

// const {incDispatch, decDispatch, rndDispatch} = bindActionCreators({
//     incDispatch: inc,
//     decDispatch: dec,
//     rndDispatch: rnd
// }, dispatch);

// const bindActionCreator = (dispatch, action) => {
//     return (...args) => dispatch(action(...args));
// };
// const incDispatch = bindActionCreator(dispatch, inc);
// const decDispatch = bindActionCreator(dispatch, dec);
// const rndDispatch = bindActionCreator(dispatch, rnd);


ReactDom.render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('root'));

// const update = () => {
//     ReactDom.render(
//         <Provider store={store}>
//         <App/>
//         </Provider>,
//         document.getElementById('root'));
// };

// update();
// store.subscribe(update);


