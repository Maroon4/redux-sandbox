import React from "react";
import {bindActionCreators} from "redux";
import {connect} from 'react-redux'
import * as actions from "../actions";

const Counter = ({counter, inc, dec, rnd}) => {
    return (
        <div className="jumbotron">
            <h2>{counter}</h2>
            <button id="dec" className="btn btn-primary btn-lg"
                 onClick={dec}>Dec</button>
            <button id="inc" className="btn btn-primary btn-lg"
                 onClick={inc}>Inc</button>
            <button id="rnd" className="btn btn-primary btn-lg"
                 onClick={rnd}>Rnd</button>
        </div>
    )
};

const  mapStateToProps = (state) => {
    return {
       counter: state
    }
};
const mapDispatchToProps = (dispatch) => {

    return  bindActionCreators(actions, dispatch);

};

export default connect(mapStateToProps, mapDispatchToProps)(Counter);