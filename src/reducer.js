

export const reducer = (state = 4, action) => {

    switch (action.type) {
        case 'RND':
            return state + action.randomV;

        case 'INC':
            return state + 1;

        case 'DEC':
            return state - 1;

        default:
            return state;
    }

};